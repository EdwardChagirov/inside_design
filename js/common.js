$(document).ready(function() {
  if ($(window).scrollTop() > 100) {
    $('.header').addClass('header--sticky');
  }

  $(window).scroll(function() {
    setTimeout(function() {
      if ($(window).scrollTop() > 100) {
        $('.header').addClass('header--sticky');
      } else {
        $('.header').removeClass('header--sticky');
      }
    }, 250);
  });

  var a = 0;
  $(window).scroll(function() {

  var oTop = $('.counters').offset().top - window.innerHeight;
    if (a == 0 && $(window).scrollTop() > oTop) {
      $('.counter .counter__decimal').each(function() {
        var $this = $(this),
          countTo = $this.attr('data-count');

        $({
          countNum: $this.text()
        }).animate({
            countNum: countTo
          },
          {
            duration: 2000,
            easing: 'swing',
            step: function() {
              $this.text(Math.floor(this.countNum));
            },
            complete: function() {
              $this.text(this.countNum);
            }
          });
      });
      a = 1;
    }
  });
});